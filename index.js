const {exec} = require('child_process');
const moment = require('moment')
const fs = require('fs')
const secrets = JSON.parse(fs.readFileSync('./secrets.json', 'utf8'))

var Twitter = require('./twitter').Twitter;

var twitter = new Twitter(secrets);

// function replyWithMusic() {
//     twitter.postTweet({
//         status: 'test post please ignore5'
//     }, error, success);
// }

// const express = require('express')
// const app = express()
// const port = 3000

// const TwitterEvents = Object.freeze({
//     tweet_create_events: 'tweet_create_events'
// })

// app.post('/webhook/twitter', OnTwitterWebhook)

// function OnTwitterWebhook(req, res) {
//     res.send('Hello World!')
// }

// app.listen(port, () => console.log(`Example app listening on port ${port}!`))

// function registerWebhookWithTwitter() {
//     const webhook = 'https://emojidrums.adenflorian.com/webhook/twitter'
//     axios.post('/webhooks.json?url=' + webhook)
// }
theThing()

setInterval(() => {
    theThing()
}, 30 * 1000)


// start loop

const startTime = moment()

const processedTweetIds = []



function theThing() {
    twitter.getMentionsTimeline({}, error, (tweets, limits) => {
        console.log(`totalTweets: `, tweets.length)

        // filter out tweets that happened before we started
        const tweetsAfterStartTime = tweets.filter(x => moment(x.created_at).isAfter(startTime))
        console.log(`tweetsAfterStartTime: `, tweetsAfterStartTime.length)
        const unprocessedTweets = tweetsAfterStartTime.filter(x => processedTweetIds.includes(x.id) === false)
        console.log(`unprocessedTweets: `, unprocessedTweets.length)
        if (unprocessedTweets.length === 0) return
        const tweetToProcess = unprocessedTweets[0]
        console.log(`tweet: ${tweetToProcess.text}`)
        processedTweetIds.push(tweetToProcess.id)
        console.log(`processedTweetIds: `, processedTweetIds)
        // console.log(`tweets: `, tweets)
        // const created_at = tweets[0].created_at
        // console.log(`created_at: `, created_at)
        // console.log(`created_at: `, moment(created_at))
        // console.log(`startTime: `, startTime)


        const user = tweets[0].user
        const userScreenName = user.screen_name
        const responseText = `@${userScreenName} ok, so check it:`
        console.log(`responseText: ${responseText}`)


        foo(tweetToProcess.text, responseText)
    });
}




const path = require('path');
const Twit = require('Twit');

// you will need an object with your Twitter data
// const myTweetObj = {
//     content: 'This is the text of the tweet',
//     twitter_handle: 'EmojiDrum',
// };

function uploadVideoAndTweetAsync(myTweetObj) {
    return new Promise((resolve, reject) => {
        _twitterVideoPub(myTweetObj, resolve, reject)
    })
}

// This function is called from within a promise,
// so I pass it 'resolve' and 'reject' so my intentions
// are clear to you, the reader. This isn't 
// necessary though, as you can access the 'resolve' 
// and 'reject' living in the outer scope w/o passing
// them to the function as arguments
function _twitterVideoPub(myTweetObj, resolve, reject) {
    console.log('_twitterVideoPub: ', myTweetObj)

    const T = new Twit({
        consumer_key: secrets.consumerKey,
        consumer_secret: secrets.consumerSecret,
        access_token: secrets.accessToken,
        access_token_secret: secrets.accessTokenSecret,
    });

    const PATH = path.join(__dirname, myTweetObj.vidPath);

    T.postMediaChunked({file_path: PATH}, function (err, data, response) {

        const mediaIdStr = data.media_id_string;
        const meta_params = {media_id: mediaIdStr};

        T.post('media/metadata/create', meta_params, function (err, data, response) {

            if (!err) {
                const params = {status: myTweetObj.content, media_ids: [mediaIdStr]};

                T.post('statuses/update', params, function (err, tweet, response) {
                    console.log(tweet);
                    const base = 'https://twitter.com/';
                    const handle = myTweetObj.twitter_handle;
                    const tweet_id = tweet.id_str;
                    resolve({
                        live_link: `${base}${handle}/status/${tweet_id}`
                    });

                }); // end '/statuses/update'

            } // end if(!err)

        }); // end '/media/metadata/create'

    }); // end T.postMedisChunked
}

//Callback functions
var error = function (err, response, body) {
    console.log('ERROR: ', {
        err,
        headers: response.headers,
        body
    });
};
var success = function (body, limits, headers) {
    console.log({
        body,
        limits
    });
};

// foo('👢🐱👢👏');

function foo(emojiString, text) {
    console.log(`emojiString: ${emojiString}`)
    const soundNames = [...emojiString].map(toSoundNames)
    console.log(`soundNames: ${soundNames}`)
    const filteredSoundNames = soundNames.filter(x => x !== undefined)
    console.log(`filteredSoundNames: ${filteredSoundNames}`)
    if (filteredSoundNames.length === 0) {
        console.warn(`no valid emoji in emojiString: ${emojiString}`)
        console.warn(`ignoring`)
        return;
    }
    bar(filteredSoundNames, fileName => {
        return uploadVideoAndTweetAsync({
            content: text,
            twitter_handle: 'EmojiDrum',
            vidPath: fileName
        })
    });
}

function toSoundNames(emoji) {
    const emojiToSoundName = Object.freeze({
        '👢': 'kick',
        '🐱': 'clap',
        '👏': 'clap'
    })

    return emojiToSoundName[emoji];
}

function bar(soundNames, cb) {
    let command = 'ffmpeg '

    soundNames.forEach(x => {
        command += `-i clips/${x}-1.wav `
    });

    command += '-filter_complex "'

    soundNames.forEach((_, i) => {
        command += `[${i}:0]`
    });

    command += `concat=n=${soundNames.length}:v=0:a=1[out]" -map "[out]" output.wav -y`

    console.log(command)

    exec(command, (err, stdout, stderr) => {
        if (err) {
            console.error(`exec error: ${err}`);
            return;
        }

        console.log(`stderr: ${stderr}`);
        console.log(`stdout: ${stdout}`);

        exec('ffmpeg -loop 1 -y -i EDSAVATAR.png -i output.wav -shortest -acodec copy -vcodec mjpeg result.avi -y', (err, stdout, stderr) => {
            if (err) {
                console.error(`exec error: ${err}`);
                return;
            }

            console.log(`stderr: ${stderr}`);
            console.log(`stdout: ${stdout}`);

            const finalVideoFileName = 'final.mp4';

            const nextCommand = `ffmpeg -i result.avi -pix_fmt yuv420p -vcodec libx264 -vf "scale=640:trunc(ow/a/2)*2" -acodec aac -vb 1024k -minrate 1024k -maxrate 1024k -bufsize 1024k -ar 44100 -strict experimental -r 30 ${finalVideoFileName} -y`

            console.log(nextCommand);
            exec(nextCommand, (err, stdout, stderr) => {
                if (err) {
                    console.error(`exec error: ${err}`);
                    return;
                }

                console.log(`stderr: ${stderr}`);
                console.log(`stdout: ${stdout}`);

                setTimeout(() => {
                    cb(finalVideoFileName)
                }, 5000)
            });
        });
    });
}
