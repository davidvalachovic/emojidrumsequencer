twurl authorize --consumer-key key --consumer-secret secret

twurl -H upload.twitter.com -X POST "/1.1/media/upload.json" --file "out.mp4" --file-field "media"

ffmpeg -i result.avi -pix_fmt yuv420p -vcodec libx264 -vf "scale=640:trunc(ow/a/2)*2" -acodec aac -vb 1024k -minrate 1024k -maxrate 1024k -bufsize 1024k -ar 44100 -strict experimental -r 30 out.mp4
